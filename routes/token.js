var express = require('express');
var router = express.Router();
var tokenController = require('../controllers/token');

router.get('/confirmation/t:token', tokenController.confirmationGet);

module.exports = router;