
var mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');

describe('testing bicis', function(){
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.connection;

        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('connected');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        })
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacía', (done) => {            
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            })
        })
    })

    describe('Bicicleta.add', () => {
        it('agrega solo una bici',(done)=>{
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "fea"});
            Bicicleta.addListener(aBici, function(err, newBici){
                if (err) console.log (err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);
                    Bicicleta.findByCode(1, function (error, targetBici){
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);

                        done();
                    });
                });
            });
        });
    });

});

/*beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis',() =>{
    it('comienza vacía', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add',()=>{
    it('add 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(420,'azul', 'sin ruedas', [-3.6345869, -59.805199]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findBy',()=>{
    it('debe devolver bici con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(420,'azul', 'sin ruedas', [-3.6345869, -59.805199]);
        var anotherBici = new Bicicleta(11,'verde', 'con una sola rueda', [-3.6945869, -59.835199]);
        Bicicleta.add(aBici);
        Bicicleta.add(anotherBici);

        var targetBici = Bicicleta.findById(420);

        expect(targetBici.id).toBe(420);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});

describe('Bicicleta.removeById',()=>{
    it('debe devolver agregar y eliminar bici con id 5', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(5,'rosa', 'sin pedales', [-3.8945869, -59.735199]);
        Bicicleta.add(aBici);

        var targetBici = Bicicleta.findById(5);
        expect(targetBici.id).toBe(5);
        expect(Bicicleta.allBicis.length).toBe(1);
        
        Bicicleta.removeById(5);
        expect(Bicicleta.allBicis.length).toBe(0);

    });
});*/