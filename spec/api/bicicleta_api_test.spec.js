var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1,'negro', 'urbana', [-3.8945869, -59.735199]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {

            var a = new Bicicleta(10,'rojo', 'urbana', [-34, -54]);
            Bicicleta.add(a);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "transparente", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/10/update',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("transparente");
                done();
            });
        });
    });

    describe('POST BICICLETAS /delete', () => {
        it('Status 200', (done) => {

            var a = new Bicicleta(10,'rojo', 'urbana', [-34, -54]);
            Bicicleta.add(a);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10}';
            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/10/delete',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                done();
            });
        });
    });
});
