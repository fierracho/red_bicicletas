var mymap = L.map('mapid').setView([-38.647520, -59.606208], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy: <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

L.marker([-38.643298, -59.599490]).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success:function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
})
