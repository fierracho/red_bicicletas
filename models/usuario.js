var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
const saltRounds = 10;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const Token = require('../models/token');
const mailer = require('../mailer/mailer');
var Schema = mongoose.Schema;


const validateEmail = function(email){
    const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'poné tu nombre']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'poné el mail'],
        lowercase:true,
        unique: true,
        validate:[validateEmail, 'escribí bien pa'],
        match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g]
    },
    password: {
        type:String,
        required: [true, 'poné el pass']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verified: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, { message: 'user ya existe'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(db) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verify',
            text: 'Hola,\n\n' + 'link: ' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token + '.\n'
        }

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return console.log(err.message); }

            console.log('se mandó un mail a ' + email_destination);
        })
    })
}

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if (err) { return cb(err); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'reset pass',
            text: 'Hola,\n\n' + 'link: ' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return cb(err); }

            console.log('se mandó un mail a ' + email_destination);
        });

        cb(null);
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if (resullt) {
                callback(err, result)
            } else {
                console.log('-----CONDITION-----');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'no name';
                values.verificado = true;
                values.password = condition._json.etag;
                console.log('-----VALUES-----');
                console.log(values);
                self.create(values, (err, result) => {
                    if (err) {console.log(err);}
                    return callback (err, result)
                })
            }
        })
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if (resullt) {
                callback(err, result)
            } else {
                console.log('-----CONDITION-----');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'no name';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                console.log('-----VALUES-----');
                console.log(values);
                self.create(values, (err, result) => {
                    if (err) {console.log(err);}
                    return callback (err, result)
                })
            }
        })
};
module.exports = mongoose.model('Usuario', usuarioSchema);